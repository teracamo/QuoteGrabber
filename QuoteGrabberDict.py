HSI_Bloomberg = {
    'name': "HSI_Bloomberg",
    'url': "https://www.bloomberg.com/quote/HSI:IND",
    'xpathprice': "//div[@class='price']",
    'xpathdatetime': "id('content')/div/div/div[1]/div/div[5]",
    'datetimeFormat': " As of %I:%M %p EDT %m/%d/%Y ",
    'javaProtected': False
}

HK_00136_MONEY18 = {
    'name': "HK00136_MONEY18",
    'url': "http://money18.on.cc/eng/info/liveinfo_quote.html?symbol=00136",
    'xpathprice': "id('highlight')//td[@class='price']/span[1]",
    'xpathdatetime': "id('lastUpdated')//span[@class='timeStamp']",
    'datetimeFormat': "%Y/%m/%d %H:%M",
    'javaProtected': True
}

HSI_MONEY18 = {
    'name': "HSI_MONEY18",
    'url': "http://money18.on.cc/info/liveinfo_idx.html",
    'xpathprice': "id('indexList')/div/div[2]//td[2]",
    'xpathdatetime': "id('lastUpdated')//span[@class='timeStamp']",
    'datetimeFormat': "%Y/%m/%d %H:%M",
    'javaProtected': True
}
