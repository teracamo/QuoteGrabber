#/bin/python
import os
import csv
import time
import lxml.html as xhtml
import requests
import datetime
import dryscrape

import QuoteGrabberDict


import logging
logging.basicConfig(filename='./Logs/QuoteGrabber_%s.txt'%(datetime.datetime.strftime(datetime.datetime.now(), "%Y%m%d"))
                    ,level=logging.INFO, format='%(asctime)s %(message)s')

def GrabPrice(url, xpathprice, xpathtime):
    r = requests.get(url)
    tree = xhtml.fromstring(r.content)
    p = tree.xpath(xpathprice)
    if (len(p) > 0):
        price = p[0].text_content()
    else:
        price = "-1"
        logging.warning("Failed to obtain price from %s."%url)
    t = tree.xpath(xpathtime)
    if (len(t) > 0):
        time = t[0].text
    else:
        time = datetime.datetime.now().strftime("%Y/%m/%d %I:%M%p")
        logging.warning("Failed to obtain time from %s" % url)
    return [time, price]

def JAVAGrabPrice(url, xpathprice, xpathtime):
    global session

    session.visit(url)
   
    q = session.at_xpath(xpathprice)
    if (q):
        price = q.text()
    else:
        price = "-1"
        logging.warning("Failed to obtain price from %s."%url)
    q = session.at_xpath(xpathtime)
    if (q):
        time = q.text()
    else:
        time = datetime.datetime.now().strftime("%Y/%m/%d %I:%M%p")
        logging.warning("Failed to obtain time from %s"%url)
    return [time, price]

def WriteDatePrice():
    global targets, lasttimestamp
    for stockDict in targets:
        outputfilename = "./Output/" + stockDict['name'] + ".csv"
        if (stockDict['javaProtected']):
            [t, price] = JAVAGrabPrice(stockDict['url'], stockDict['xpathprice'], stockDict['xpathdatetime'])
        else:
            [t, price] = GrabPrice(stockDict['url'], stockDict['xpathprice'], stockDict['xpathdatetime'])
        if (lasttimestamp == datetime.datetime.strptime(t, stockDict['datetimeFormat'])):
            return False
        else:
            if (not os.path.exists(outputfilename)):
                logging.info("Creating file %s" % outputfilename)
                try:
                    f = open(outputfilename, 'w')
                    f.close()
                except():
                    logging.warning("Creating file %s failed!" % outputfilename)
                    logging.info("Terminating.")
                    pass

            lasttimestamp = datetime.datetime.strptime(t, stockDict['datetimeFormat'])
            f = open(outputfilename, 'ab')
            writer = csv.DictWriter(f, fieldnames=['Timestamp', 'Value'])
            datetimestr = datetime.datetime.strftime(lasttimestamp, "%Y/%m/%d %I:%M%p")
            writer.writerow({'Timestamp': datetimestr, 'Value': float(price.replace(',', ''))})
            f.close()
        time.sleep(3)


    return True



def FindClosestTradingTime():
    localtime = datetime.datetime.now()
    weekday = localtime.weekday()


    starttime = datetime.datetime.now().replace(hour=9, minute=30, second=0, microsecond=0)
    endtime = datetime.datetime.now().replace(hour=16, minute=10, second=0, microsecond=0)

    offset = 0
    nextworkingday = datetime.datetime.now()
    nextworkingday = nextworkingday.replace(hour=9, minute=30, second=0, microsecond=0)
    if (weekday >= 4): # if Friday, Sat or Sun
        offset = (-weekday)%7
    else:
        offset = 1

    dend = localtime - endtime
    dstart = localtime - starttime
    if(dend.total_seconds() < 0 and dstart.total_seconds() >= 0):
        return localtime # Within trading period
    else:
        nextworkingday = nextworkingday.replace(day=localtime.day + offset, minute=30, hour=9, second=0)
        return nextworkingday # Not within trading period

def main():
    global targets, lasttimestamp, session

    logging.info("Starting xvfb...")
    dryscrape.start_xvfb()
    session = dryscrape.Session()

    targets = []
    #targets.append(QuoteGrabberDict.HSI_MONEY18)
    targets.append(QuoteGrabberDict.HK_00136_MONEY18)
    targets.append(QuoteGrabberDict.HSI_Bloomberg)

    while(True):
        lasttimestamp = None
        closestTime = FindClosestTradingTime()
        localtime = datetime.datetime.now()
        dtime = datetime.datetime.now() - FindClosestTradingTime()
        if (dtime.total_seconds() < 0):
            logging.info("Not a working day, waiting till %s."%FindClosestTradingTime().ctime())
            time.sleep(abs(dtime.total_seconds()))

        try:
            if (WriteDatePrice()):
                time.sleep(60)
            else:
                time.sleep(1)
        except():
            logging.warning("Something went wrong during write.")
            pass

    pass

if __name__ == '__main__':
    logging.info("Start logging quotes...")
    main()
