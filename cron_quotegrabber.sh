#!/bin/sh

SCRIPT_DIR=$(dirname $0)
CURRENT_RUNNING_PIDs=`pgrep -f QuoteGrabber.py|wc -l`
echo $CURRENT_RUNNING_PIDs

# if no process is running, run it
if [ $CURRENT_RUNNING_PIDs -eq 0 ]; then
	cd $SCRIPT_DIR;
	echo "RUNNING ${SCRIPT_DIR}/QuoteGrabber.py"
	python QuoteGrabber.py
fi
